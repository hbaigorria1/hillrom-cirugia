<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{
		//$this->load->library('GetResponse'); 
		//$this->getresponse->enterprise_domain = 'cloud.oxford.com.ar';
		
		//$result = $this->getresponse->getContacts();
		//$data['contactos']= $result;
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
        
        
	    
		//desde aquí también podemos setear el título
		
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();


		if(empty($this->session->userdata('language'))){
			$this->session->set_userdata('language', $_POST['language']);
		}
		$lenguage_session = $this->session->userdata('language');
		//Load form helper
		$this->load->helper('form');
		
		$load_pt = $this->lang->load('web_lang','portuguese');
		$load_es = $this->lang->load('web_lang','spanish');
		$load_en = $this->lang->load('web_lang','english');
		//Get the selected language
		//$language = $this->input->post('language');
		if(!empty($lenguage_session)){
			$language = $lenguage_session;
		}
			

		$leng = $this->config->item('language_abbr');
		//Choose language file according to selected lanaguage
		//print_r($language);
		//exit;
		if($language == "portuguese"):
			$this->lang->load('web_lang','portuguese');
			$data['shortname'] = "pt";
			$data['language'] = $language;
		elseif ($language == "spanish"):
			$this->lang->load('web_lang','spanish');
			$data['shortname'] = "es";
			$data['language'] = $language;
		elseif ($language == "english"):
			$load_en = $this->lang->load('web_lang','english');
			$data['shortname'] = "en";
			$data['language'] = $language;
		elseif ($language == "german"):
			$load_en = $this->lang->load('web_lang','german');
			$data['shortname'] = "de";
			$data['language'] = $language;
		else:
			
            if ($leng == 'ar'){
            	$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = "spanish";
			}

			if ($leng == 'cl'){
            	$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = "spanish";
			}

			if ($leng == 'mx'){
            	$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = "spanish";
			}

			if ($leng == 'br'){
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = "portuguese";
			}

			if ($leng == 'us'){
				$load_en = $this->lang->load('web_lang','english');
				$data['shortname'] = "en";
				$data['language'] = "english";
			}

		endif;

		$data["config"] = $this->page_model->get_config($data['language']);
		$this->template->write('title', $data["config"][0]->title, TRUE);

        $data["quirofano"] = $this->page_model->get_quirofano($_GET['quiofano'],$data['language']);
        $data["procedimiento"] = $this->page_model->get_procedimiento($_GET['procedmiento'],$data['language']);

        $data["producto"] = $this->page_model->get_product_id($_GET['ver'],$data['language']);
        $data["posicionamientos_producto"] = $this->page_model->get_componentes_producto_posicionamiento($_GET['ver'],$data["procedimiento"][0]->uniq, $data['language']);

        $data["posicionamientos_pads"] = $this->page_model->get_pads_posicionamiento($data["procedimiento"][0]->uniq, $data['language']);
        
        $data["html"] = $this->page_model->buildHTMLTemplate($_GET['ver'], $data['language'], $_GET['procedmiento']);
    
		$this->template->write_view('content', 'layout/producto/main', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
	    
		$this->template->write_view('footer', 'layout/footer-producto');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function add()
	{	

		session_start();
		if(!empty($_POST["action"])) {
			switch($_POST["action"]) {
				case "add":
					if(!empty($_SESSION["cart"])):
						if(!in_array($_POST['product_id'],$_SESSION["cart"])):
							$arrayAdd = array($_POST['product_id'] => array($_POST['posicion_id'] => $_POST['posicion_id']));
							$_SESSION["cart"] = $this->array_merge_recursive_new($_SESSION["cart"],$arrayAdd);
						endif;
					else:
						$_SESSION["cart"] =array($_POST['product_id'] => array($_POST['posicion_id'] => $_POST['posicion_id']));
					endif;
				break;
				case "remove":
					if(!empty($_SESSION["cart"])) {
						foreach($_SESSION["cart"] as $product_id => $posiciones) {
							if($posiciones > 0):
								foreach($posiciones as $key) {
									unset($_SESSION['cart'][$_POST['product_id']][$_POST['posicion_id']]);
								}
							else:
								unset($_SESSION['cart'][$_POST['product_id']][0]);
							endif;
						}
					}
				break;
				case "empty":
					unset($_SESSION["cart_item"]);
				break;		
			}
		}
		if(isset($_SESSION["cart"])):
			foreach($_SESSION["cart"] as $product_id => $posiciones):
			$producto = $this->page_model->get_product_id($product_id, 'spanish');
			
				foreach($posiciones as $key):
					$padkey = explode('pad_', $key);
					$opc = explode('op_', $key);
					$posicion = $this->page_model->get_posicion_id($key, 'spanish');
					$opcion = $this->page_model->get_opcion_id($opc[1], 'spanish');
					$pad = $this->page_model->get_pad_id($padkey[1], 'spanish');
					if($key == 'uniq'):
						$html .='
						<div class="gill-cart w-100 d-flex align-items-center flex-wrap">
						  <div class="img-cart" style="background:url(\''.base_url().'asset/img/uploads/'.$producto[0]->imagenes.'\')"></div>
						  <div class="detalle-cart"><p>'.$producto[0]->titulo.'</p></div>
						  <div class="btn-borrar" onClick=cartAction("remove","'.$product_id.'","'.$key.'")>
						    <i class="fas fa-trash-alt"></i>
						  </div>
						</div>
						';
					elseif(isset($padkey[1])):
						$html .='
						<div class="gill-cart w-100 d-flex align-items-center flex-wrap">';
						if(!empty($pad[0]->imagenes)):
						$html.='
						  <div class="img-cart" style="background:url(\''.base_url().'asset/img/uploads/'.$pad[0]->imagenes.'\')"></div>';
						else:
							$html.='
						  <div class="img-cart" style="background:url(\''.base_url().'asset/img/img-componente-prdet.png\')"></div>';
						endif;
						  $html.='<div class="detalle-cart"><p>'.$pad[0]->nombre.'<br><small>'.$producto[0]->titulo.' - '.$pad[0]->posicion.'</small></p></div>
						  <div class="btn-borrar" onClick=cartAction("remove","'.$product_id.'","'.$key.'")>
						    <i class="fas fa-trash-alt"></i>
						  </div>
						</div>
						';
					elseif(isset($opc[1])):
						$html .='
						<div class="gill-cart w-100 d-flex align-items-center flex-wrap">
						';
						if(!empty($opcion[0]->imagenes)):
						$html.='
						  <div class="img-cart" style="background:url(\''.base_url().'asset/img/uploads/'.$opcion[0]->imagenes.'\')"></div>';
						else:
							$html.='
						  <div class="img-cart" style="background:url(\''.base_url().'asset/img/img-componente-prdet.png\')"></div>';
						endif;
						  $html.='
						  <div class="detalle-cart"><p>'.$opcion[0]->nombre.'<br><small>'.$producto[0]->titulo.' - '.$opcion[0]->posicion.'</small></p></div>
						  <div class="btn-borrar" onClick=cartAction("remove","'.$product_id.'","'.$key.'")>
						    <i class="fas fa-trash-alt"></i>
						  </div>
						</div>
						';
					else:
					$html .= '
					  <div class="gill-cart w-100 d-flex align-items-center flex-wrap">
					    <div class="img-cart" style="background:url(\''.base_url().'asset/img/uploads/'.$posicion[0]->imagen.'\')"></div>
					    <div class="detalle-cart"><p>'.$posicion[0]->nombre.'<br><small>'.$producto[0]->titulo.'</small></p></div>
					    <div class="btn-borrar" onClick=cartAction("remove","'.$product_id.'","'.$key.'")>
					      <i class="fas fa-trash-alt"></i>
					    </div>
					  </div>
					';
					endif;

				endforeach;
			endforeach;
			$html .='<div class="finalizar">
			    <a href="'.base_url().$this->config->item('language_abbr').'/contacto/">Consultar</a>
			  </div>
			</div>';
		endif;
		foreach($_SESSION['cart'] as $product => $array):
			$contador += count($array);
		endforeach;
		$response = array(
			   'html'=>$html,
			   'contador'=> $contador
			  );					
		echo json_encode($response);

		//echo $html;
		
	}
	
	private function array_merge_myway()
	{
	    $output = array();

	    foreach(func_get_args() as $array) {
	        foreach($array as $key => $value) {
	            $output[$key] = isset($output[$key]) ?
	                array_merge($output[$key], $value) : $value;
	        }
	    }

	    return $output;
	}

	private function array_merge_recursive_new()
	{
	    $arrays = func_get_args();
	    $base = array_shift($arrays);

	    foreach($arrays as $array) {
	        reset($base);
	        while(list($key, $value) = @each($array)) {
	            if(is_array($value) && @is_array($base[$key])) {
	                $base[$key] = $this->array_merge_recursive_new($base[$key], $value);
	            }
	            else {
	                $base[$key] = $value;
	            }
	        }
	    }

	    return $base;
	}
	private function array_merge_recursive_distinct()
	{
	    $arrays = func_get_args();
	    $base = array_shift($arrays);

	    if(!is_array($base)) $base = empty($base) ? array() : array($base);

	    foreach($arrays as $append) {
	        if(!is_array($append)) $append = array($append);
	        foreach($append as $key => $value) {
	            if(!array_key_exists($key, $base) and !is_numeric($key)) {
	                $base[$key] = $append[$key];
	                continue;
	            }
	            if(is_array($value) or is_array($base[$key])) {
	                $base[$key] = $this->array_merge_recursive_distinct($base[$key], $append[$key]);
	            }
	            else if(is_numeric($key))
	            {
	                if(!in_array($value, $base)) $base[] = $value;
	            }
	            else {
	                $base[$key] = $value;
	            }
	        }
	    }

	    return $base;
	}

	public  function test()
	{
		/**
		 * Merge two dimensional arrays my way
		 *
		 * Will merge keys even if they are of type int
		 *
		 * @param  array $array1 Initial array to merge.
		 * @param  array ...     Variable list of arrays to recursively merge.
		 *
		 * @author Erik Pettersson <mail@ptz0n.se>
		 */
		

		$first = array(
		    1327006800 => array('avgresponse' => 1803),
		    1327003200 => array('avgresponse' => 1453)
		);

		$second = array(
		    1327006800 => array('pageviews' => 123),
		    1327003200 => array('pageviews' => 345)
		);

		/**
		 * Test functions
		 *
		 */
		echo '<pre>';

		// Recursive array merge will not work as expected
		echo 'array_merge_recursive fails: ';
		print_r(array_merge_recursive($first, $second));

		// Let's try our function out
		echo "\n", 'array_merge_myway success: ';
		print_r($this->array_merge_myway($first, $second));

		// Let's try another one
		echo "\n", 'array_merge_recursive_new works too: ';
		print_r($this->array_merge_recursive_new($first, $second));

		// Let's try another one
		echo "\n", 'array_merge_recursive_distinct also works: ';
		print_r($this->array_merge_recursive_distinct($first, $second));
	}
}
