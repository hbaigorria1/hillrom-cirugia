<?php $leng = $this->config->item('language_abbr');
        if ($leng == 'ar'){
          $this->lang->load('web_lang','spanish');
      $shortname = "es";
      $language = "spanish";
    }
    $tipos = $this->page_model->get_categories($language);
    if ($leng == 'br'){
      $this->lang->load('web_lang','portuguese');
      $shortname = "pt";
      $language = "portuguese";
    }

?>
<div class="header" class="w-100" style="text-align:left;">
  <div class="w-100" style=" background-color:#003399;">
    <div class="container">
      <div class="row m-0 w-100">
          <div class="col-6 logo d-inline-flex align-items-center justify-content-center" style="position:relative;">
          <span style="margin-right: auto;color:#fff; font-weight:bold; padding-left:0px;font-family:Arial;font-size:13px;"><?=$this->lang->line('hlbax')?></span>
          </div>
            <div class="col-6  d-flex align-items-center justify-content-center">
              <a href="<?=base_url()?>" style="display:inline-block;margin-left: auto;">
                  <img style="height:15px;" src="https://www.descubrehillrom.com/asset/img/LogoBaxter-01.png" class="logo-wexll">
              </a>
            </div> 
      </div>
    </div>
  </div>
  <div class="menu">
    <div class="config" onclick="showcart()">
      <p class="contador" style="display:none;"></p>
      <i class="fas fa-cogs"></i>
    </div>
    <div id="hamburger" onclick="this.classList.toggle('open');">
      <svg width="50" height="50" viewBox="0 0 100 100">
        <path class="line line1" d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
        <path class="line line2" d="M 20,45 H 80" />
        <path class="line line3" d="M 20,60 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
      </svg>
      <div class="menu-content">
        <ul class="idioma">
          <li><a href="<?php echo base_url().'ar'.uri_string()?><?php if(isset($_GET['categoria'])):?>?categoria=<?=$_GET['categoria']?><?php endif; ?><?php if(isset($_GET['ver'])):?>?ver=<?=$_GET['ver']?><?php if(isset($_GET['procedmiento'])):?>&procedmiento=<?=$_GET['procedmiento']?><?php endif; ?><?php else: ?><?php if(isset($_GET['procedmiento'])):?>?procedmiento=<?=$_GET['procedmiento']?><?php endif; ?><?php endif; ?><?php if(isset($_GET['quiofano'])):?>&quiofano=<?=$_GET['quiofano']?><?php endif; ?><?php if(isset($_GET['posicion'])):?>&posicion=<?=$_GET['posicion']?><?php endif; ?>"<?php if($language == 'spanish'):?> class="active" <?php endif; ?>><?=$this->lang->line('leng_es')?></a></li>
          <li><a href="<?php echo base_url().'br'.uri_string()?><?php if(isset($_GET['categoria'])):?>?categoria=<?=$_GET['categoria']?><?php endif; ?><?php if(isset($_GET['ver'])):?>?ver=<?=$_GET['ver']?><?php if(isset($_GET['procedmiento'])):?>&procedmiento=<?=$_GET['procedmiento']?><?php endif; ?><?php else: ?><?php if(isset($_GET['procedmiento'])):?>?procedmiento=<?=$_GET['procedmiento']?><?php endif; ?><?php endif; ?><?php if(isset($_GET['quiofano'])):?>&quiofano=<?=$_GET['quiofano']?><?php endif; ?><?php if(isset($_GET['posicion'])):?>&posicion=<?=$_GET['posicion']?><?php endif; ?>"<?php if($language == 'portuguese'):?> class="active" <?php endif; ?>><?=$this->lang->line('leng_pt')?></a></li>
        </ul>
        <ul>
          <li><a href="<?=base_url()?>">Inicio</a></li>
          <?php foreach($tipos as $tp): ?>
            <?php if($tp->uniq == 4): ?>
                <li>
                  <a href="<?=base_url().$this->config->item('language_abbr')?>/procedimientos?categoria=4"><?=$tp->nombre?></a>
                </li>
              <?php else: ?>
                <li>
                  <a href="<?=base_url().$this->config->item('language_abbr')?>/configuracion-avanzada"><?=$tp->nombre?></a>
                </li>
            <?php endif; ?>
          <?php endforeach; ?>
          <li><a href="<?=base_url().$this->config->item('language_abbr')?>/contacto/"><?=$this->lang->line('lbl_menu_contacto')?></a></li>
        </ul>
      </div>
    </div>
    <div id="cart" class="content-cart">
      <p style="margin: 0;font-size: 10px;">Ningun producto solicitado</p>
    </div>
  </div>
</div>

