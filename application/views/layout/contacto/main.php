<style type="text/css">

	html.html-hidde-overflow, body.html-hidde-overflow {overflow: auto !important;}
</style>
<section class="home-section" style="background: #fff;max-height:inherit;height: auto;">
	<div style="margin-bottom: auto;margin-top: 40px;">
		<h4 style="margin-bottom: 50px;"><?=$this->lang->line('lbl_menu_contacto')?></h4>
	</div>
    <div class="btns-home w-100 d-flex align-items-center justify-content-center" style="max-width: 380px;margin: 0;">
    	<iframe id="pardot_gated" src="<?=$this->lang->line('lbl_form_contacto')?>" width="100%" height="1000" type="text/html" frameborder="0" allowtransparency="true" style="border: 0;height: 1000px;"></iframe>
    </div>
</section>