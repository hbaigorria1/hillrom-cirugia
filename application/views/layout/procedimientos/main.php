<section class="home-section" style="background: url('<?php echo base_url() ?>asset/img/back-tipo-proc-02.jpg')">
	<div>
		<h4><?=$this->lang->line('lbl_tipos_procedimientos')?></h4>
		<p><?=$this->lang->line('lbl_tipos_procedimientos_desc')?></p>
	</div>
    <div class="btns-home w-100 d-flex align-items-center justify-content-center flex-wrap">
    	<?php foreach($subcategories as $sub): ?>
			<a href="<?=base_url().$this->config->item('language_abbr')?>/quirofanos?procedmiento=<?=$sub->uniq?>" class="botton-home" style="background:<?=$sub->color?>;">
				<h3><?=$sub->nombre?></h3>
			</a>
		<?php endforeach; ?>
    </div>
</section>
<div class="tori-robot">
	<div class="box-content-robot">
		<div class="close-roboto-box">X</div>
		<p id="intro"></p>
	</div>
	<img src="<?=base_url()?>asset/img/tori-icon.png" class="img-fluid max-w-img-tori">
	<div class="number">1</div>
</div>