<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_blocks extends CI_Controller {
	 
	function __construct()
	{
       parent::__construct();
       
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('form');
	   $this->load->helper('url');
	   $this->load->library('form_validation');

	   // Load session library
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/mailing_blocks.js');
	    
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador - Pauny', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();

		// getting email_id from uri
		$email_id = $this->uri->segment(3);
		
		// $data["mailing_info"] = $this->page_model->get_mailing_id($email_id);
						
		// fixing order cuando no tiene nada.
		$this->page_model->fix_orden_nulo($email_id);
				
		
		// save email_id into session
		$this->session->set_userdata('email_id', $email_id);

		$info =  $this->page_model->get_mailings_blocks($email_id);
		$data['info'] = $info;
		
		//el contenido de nuestro formulario estar� en views/registro/formulario_registro,
		//de esta forma tambi�n podemos pasar el array data a registro/formulario_registro
	    $this->template->write_view('content', 'layout/productos_blocks/list', $data, TRUE); 
	    
		//la secci�n footer ser� el archivo views/registro/footer_template
	    //$this->template->write_view('footer', 'layout/footer');   
	    
		//con el m�todo render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

		public function update_order_banner($data)
        {
				$orden = explode(',',$data['orden']);
				$ids = explode(',',$data['ids']);
				$i=0;
				foreach ( $ids as $id ){
					$this->db->set('orden', $orden[$i]);
					$this->db->where('id', $id);
					$this->db->update('productos_modulos');
					$i++;
				}
        }
        
	// ---	
	// Update orders
	// ---
	public function update_order()
	{
		$data = array(
			'orden' => $_POST['orden'],
			'ids' => $_POST['ids']
		);
        
		$this->page_model->update_order_banner($data);
	}
	
	// --
	// Update mailing
	// --
	public function update(){
		if (isset($this->session->userdata['logged_in'])) {

			$data = array(
				'azana' => $_POST['azana'],
				'pais_id' => $_POST['pais_id'],
				'subject' => $_POST['subject'],
			);
			$this->page_model->update_mailing($data);
			redirect('productos/');
		}else{
			redirect('login/');
		}
	}
	
	
	// sube bloque
	public function subir_bloque()
	{
		// arreglo los que tengan orden nulo
		$email_id = $this->session->userdata('email_id');
		$this->page_model->fix_orden_nulo($email_id);
		
		
		print "en construccion -> ".$email_id;
		exit;		
	}
	
	// baja bloque
	public function bajar_bloque()
	{
		// arreglo los que tengan orden nulo
		$email_id = $this->session->userdata('email_id');
		$this->page_model->fix_orden_nulo($email_id);
				
		
		print "en construccion";
		exit;
	}
	
	// ---	
	// Add mailing block
	// ---
	public function select_module()
	{
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/usuarios.js');
		
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();

		// --
		// Getting available Modules
		$info =  $this->page_model->get_all_modules();
		$data['modules']=$info;		
		$data['procedmiento'] =  $this->page_model->get_sub_categorias_all();
		
		$this->template->write_view('content', 'layout/productos_blocks/select_module', $data, TRUE); 
		$this->template->render();
	}
	
	// ---
	// adding module_id, and redirect to form
	// ---
	public function add_module()
	{
		$this->db->set("modulo_id",$_POST['id_modulo']);
		$this->db->set("email_id",$this->session->userdata('email_id'));
		$this->db->set("id_procedimiento",$_POST['id_procedimiento']);
		$this->db->set("added_at",time());
		$this->db->set("modified_at",time());
		$this->db->insert('productos_modulos');
		
		$block_id = $this->db->insert_id();
		
		// ok build form
		print "<script>alert('Modulo agregado correctamente, ahora falta configurarlo'); window.location='".base_url()."/productos_blocks/configure_block/".$block_id."/'</script>";
		exit;
	}
	
	
	
	public function configure_block()
	{
		$block_id = $this->uri->segment(3);
		
		// --
		// Saving into session
		$this->session->set_userdata('block_id', $block_id);
		
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/usuarios.js?time='.time());
			    
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
		
		// getting variables
		$data["variables"] = $this->page_model->get_block_variables($block_id);
		$data["lang"] =  $this->page_model->get_countries();
	    $data['posicionamientos'] =  $this->page_model->get_posicionamientos();
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		
		$this->template->write_view('content', 'layout/productos_blocks/configure_block', $data, TRUE); 
		$this->template->render();
	}
	
	// ---	
	// Saving configured block
	// ---
	public function save_configure_blocks()
	{
 		// ---
		// delete
		$this->db->where('modulo_id', $this->session->userdata('block_id'));
        $this->db->delete('productos_modulos_variables');

		// ---                
		// ok now insert
		foreach($_POST as $pa => $k)
		{
			$field_id = str_replace("field_","",$pa);
			$imagen = str_replace("galeria","",$pa);

			$field_id_image = intval(preg_replace('/[^0-9]+/', '', $imagen));
			
			$this->db->set('modulo_id', $this->session->userdata('block_id'));
			if(isset($field_id_image)):
				$this->db->set('variable_id', $field_id_image);		
			else:
				$this->db->set('variable_id', $field_id);		
			endif;
			$this->db->set("valor",basename($k));
			$this->db->set("added_at",time());
			$this->db->set("modified_at",time());
			$this->db->insert('productos_modulos_variables');		
		}

		// ----
		// vuelvo al manejo de bloques
		redirect('productos_blocks/index/'.$this->session->userdata('email_id').'/');		
	}
	

	public function update_config_blocks()
	{

		//busco si tiene id de lang
		$this->db->where("lang",$_POST['id_idioma']);
		$this->db->where('modulo_id', $this->session->userdata('block_id'));
		$query = $this->db->get('productos_modulos_variables');
		if($query->num_rows() > 0):
			// si existe, actualizo
	        foreach($_POST as $pa => $k)
			{
				$field_id = str_replace("field_","",$pa);
				$imagen = str_replace("galeria","",$pa);

				$field_id_image = intval(preg_replace('/[^0-9]+/', '', $imagen));
				
				$this->db->where('modulo_id', $this->session->userdata('block_id'));

				if(isset($field_id_image)):
					$this->db->set("uniq",$field_id_image);
					$this->db->where('variable_id', $field_id_image);		
				else:
					$this->db->set("uniq",$field_id);
					$this->db->where('variable_id', $field_id);		
				endif;
				
				$this->db->where("lang",$_POST['id_idioma']);

				$this->db->set("valor",basename($k));
				$this->db->set("added_at",time());
				$this->db->set("modified_at",time());
				$this->db->update('productos_modulos_variables');		
			}
			redirect('productos_blocks/index/'.$this->session->userdata('email_id').'/');
		else:
			// si no existe, inserto
	        foreach($_POST as $pa => $k)
			{
				$field_id = str_replace("field_","",$pa);
				$imagen = str_replace("galeria","",$pa);

				$field_id_image = intval(preg_replace('/[^0-9]+/', '', $imagen));
				
				$this->db->set('modulo_id', $this->session->userdata('block_id'));
				if(isset($field_id_image)):
					$this->db->set('variable_id', $field_id_image);	
					$this->db->set("uniq",$field_id_image);	
				else:
					$this->db->set("uniq",$field_id);
					$this->db->set('variable_id', $field_id);		
				endif;
				
				$this->db->set("lang",$_POST['id_idioma']);

				$this->db->set("valor",basename($k));
				$this->db->set("added_at",time());
				$this->db->set("modified_at",time());
				$this->db->insert('productos_modulos_variables');		
			}
			redirect('productos_blocks/index/'.$this->session->userdata('email_id').'/');
		endif;

	}
	
	public function remove(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->page_model->remove_mailing_block();
			redirect('productos_blocks/index/'.$this->session->userdata('email_id').'/');
		}else{
			redirect('login/');
		}
	}
	
	public function edit(){
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//a�adimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//a�adimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/usuarios.js');
		
		//la secci�n header ser� el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aqu� tambi�n podemos setear el t�tulo
		$this->template->write('title', 'Administrador - Pauny', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$info =  $this->page_model->get_mailing_id($this->uri->segment(3));		
		$data['info']=$info;
		
		$info =  $this->page_model->get_paises();
		$data['paises']=$info;		
		
		$this->template->write_view('content', 'layout/productos/edit', $data, TRUE); 
		$this->template->render();
	}
	
	// Logout from admin page
	public function logout() {
		// Removing session data
		$sess_array = array(
		'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		redirect('home/');
	}
	
	public function SendForm()
	{
		$parmsJSON = (isset($_POST['_p']))?$_POST['_p']:$_GET['_p'];
		$parmsJSON = urldecode(base64_decode ( $parmsJSON ));
		$JSON = new Services_JSON();
		$parmsJSON = $JSON->decode($parmsJSON);
		$asunto = $parmsJSON->{'asunto'};
		$mensaje = rawURLdecode($parmsJSON->{'mensaje'});
		$name = $parmsJSON->{'name'};
		$email = $parmsJSON->{'email'};
		$para = $parmsJSON->{'para'};
			
		$mAIL = new MAIL;
		$mAIL->From($email,$name);
        							
		$mAIL->AddTo($para);
		 $mAIL->Subject(utf8_encode($asunto));
									
	     $contact['message_body'] = $mensaje;
							        
		$mAIL->Html($contact['message_body']);
									
	 
		$cON = $mAIL->Connect("smtp.gmail.com", (int)465, "diego.mantovani@gmail.com", "p4t0f30p4t0f30", "tls") or die(print_r($mAIL->Result));
        $mAIL->Send($cON) ? $sent = true : $sent = false;
		$mAIL->Disconnect();
		
		
		if(!$sent) {
		 print '{"resultado":"NO","error":"'.$mAIL->Result.'"}';
		} else {
		  print '{"resultado":"OK"}';
		}

		exit;
			
	}
}
