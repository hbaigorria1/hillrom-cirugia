<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:10px;">
		<div class="listado">
			<div class="col-md-12 home-tools">
				<div class="row">
					<div class="col-xs-8 col-md-8">
						<h2>COMPONENTES DEL PRODUCTO</h2>
					</div>
					<div class="col-xs-4 col-md-4">
						<a href="<?php echo base_url()?>productos_componentes/add/?uniq=<?=$this->uri->segment(3)?>"><div class="btn btn-success btn-sm bt-save pull-right" style="margin-right:8px;">AGREGAR NUEVO</div></a>
					</div>
				</div>
			</div>
			<table id="list" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th width="40">ID</th>
						<th width="40">Imagen</th>
						<th>Codigo</th>
						<th>Componente</th>
						<th>Posicionamiento</th>
						<th>Cantidad</th>
						<th width="40">Editar</th>
						<th width="40">Eliminar</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$html='';
						foreach ( $info as $fila ){
							$posicionamiento = $this->page_model->get_posicion_id($fila->{'id_posicionamiento'});
							$componente = $this->page_model->get_componente_id($fila->{'id_componente'});
							$slider_images = explode(',' , $componente[0]->{'imagenes'});
							$html.='<tr>
								<td>'.$fila->{'id'}.'</td>
								<td><img src="'.base_url().'../asset/img/uploads/'.$slider_images[0].'" style="width:70px;"></td>
								<td>'.$componente[0]->codigo.'</td>
								<td>'.$componente[0]->nombre.'</td>
								<td>'.$posicionamiento[0]->nombre.'</td>
								<td>'.$fila->{'cantidad'}.'</td>
								<td align="center"><a href="'.base_url().'productos_componentes/edit/'.$fila->{'id'}.'/?uniq='.$this->uri->segment(3).'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
								<td align="center"><a href="#" data-href="'.base_url().'productos_componentes/remove/'.$fila->{'id'}.'/?uniq='.$this->uri->segment(3).''.$fila->{'uniq'}.'/" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
							</tr>';
						}
						echo $html;
					?>				
				</tbody>
			</table>
		</div>
	</div>
</div>
<br style="clear:both;"/>