<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			  <!--<li role="presentation"><a href="#tab2" data-toggle="tab">Subtabla relacionada</a></li>-->
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
			 <form method="post" action="<?php echo base_url()?>config/update/<?php echo $info[0]->{'id'}?>/">

			 	<!-- Nav tabs -->
			 	<ul class="nav nav-tabs">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<li class="nav-item">
			 			  <a class="nav-link <?php if($keylang == 0): ?> active <?php endif; ?>" data-toggle="tab" href="#sec<?php echo $lan->{'id'} ?>">Titulo <?php echo $lan->{'titulo'} ?></a>
			 			</li>
			 		<?php endforeach; ?>
			 	</ul>


			 	<!-- Tab panes -->
			 	<div class="tab-content">
		 			<?php foreach($lang as $keylang => $lan): ?>
			 			<div class="tab-pane lang-buttons <?php if($keylang == 0): ?> active <?php endif; ?>" id="sec<?php echo $lan->{'id'} ?>">

			 				<div class="td-input">
								<b>T&iacute;tulo:</b><br>
								<input type="text" name="title_<?php echo $lan->{'abr'} ?>" id="title" value="<?php if(isset($info[$keylang]->{'title'})) echo $info[$keylang]->{'title'} ?>">
							</div>

							<div class="td-input">
								<b>Descripci&oacute;n:</b><br>
								<input type="text" name="desc_<?php echo $lan->{'abr'} ?>" id="desc" value="<?php if(isset($info[$keylang]->{'desc'})) echo $info[$keylang]->{'desc'} ?>">
							</div>
							<input type="hidden" name="id_idioma_<?php echo $lan->{'abr'} ?>" value="<?php echo $lan->{'id'} ?>">
			 			</div>
			 		<?php endforeach; ?>
			 		<?php if(false): ?>
			 		<div class="td-input">
			 			<b>T&iacute;tulo de la p&aacute;gina:</b><br>
			 			<input type="text" name="pageTitle" id="pageTitle" value="<?php echo $info[0]->{'pageTitle'};?>">
			 		</div>
			 		<?php endif; ?>

			 		<div class="td-input">
			 			<b>Background:</b><br>
			 			<input type="text" name="galeria1_input" id="galeria1_input" class="img-input" value="<?php echo $info[0]->{'background'}?>" readonly>
			 			<div id="main_uploader">
			 				<div class="uploader-id1">
			 					<div id="uploader1" align="left">
			 						<input id="uploadify1" type="file" class="uploader" />
			 					</div>
			 				</div>
			 				<div id="filesUploaded" style="display:none;"></div>
			 				<div id="thumbHeight1" style="display:none;" >800</div>
			 				<div id="thumbWidth1" style="display:none;" >1440</div>
			 			</div>
			 			<div id="galeria1" class="upload-galeria">
			 				<?php if($info[0]->{'background'}<>''){ ?>
			 				<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $info[0]->{'background'}?>',function(){}); $('#galeria1_input').val(''); $(this).parent().remove();"></div><img src="../../asset/img/uploads/<?php echo $info[0]->{'background'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
			 				<?php } ?>
			 			</div>
			 		</div>
			 		
		 		</div>
		 		<?php if(false): ?>
				<div class="td-input">
					<b>T&iacute;tulo:</b><br>
					<input type="text" name="title" id="title" value="<?php echo $info[0]->{'title'};?>">
				</div>
                
                
				<div class="td-input">
					<b>Descripci&oacute;n:</b><br>
					<input type="text" name="desc" id="desc" value="<?php echo $info[0]->{'desc'};?>">
				</div>
                
				<div class="td-input">
					<b>Background:</b><br>
					<input type="text" name="galeria1_input" id="galeria1_input" class="img-input" value="<?php echo $info[0]->{'background'}?>" readonly>
					<div id="main_uploader">
						<div class="uploader-id1">
							<div id="uploader1" align="left">
								<input id="uploadify1" type="file" class="uploader" />
							</div>
						</div>
						<div id="filesUploaded" style="display:none;"></div>
						<div id="thumbHeight1" style="display:none;" >800</div>
						<div id="thumbWidth1" style="display:none;" >1440</div>
					</div>
					<div id="galeria1" class="upload-galeria">
						<?php if($info[0]->{'background'}<>''){ ?>
						<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $info[0]->{'background'}?>',function(){}); $('#galeria1_input').val(''); $(this).parent().remove();"></div><img src="../../asset/img/uploads/<?php echo $info[0]->{'background'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
						<?php } ?>
					</div>
				</div>
                
                
				<div class="td-input">
					<b>T&iacute;tulo de la p&aacute;gina:</b><br>
					<input type="text" name="pageTitle" id="pageTitle" value="<?php echo $info[0]->{'pageTitle'};?>">
				</div>
				<?php endif; ?>
			       
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	   <a href="<?php echo base_url()?>usuarios/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
	</div>
</div>
<br style="clear:both;"/>