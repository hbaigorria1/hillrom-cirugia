<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			  <!--<li role="presentation"><a href="#tab2" data-toggle="tab">Subtabla relacionada</a></li>-->
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
				
			 <form method="post" action="<?php echo base_url()?>productos/save/">
 
				<!-- Nav tabs -->
			 	<ul class="nav nav-tabs">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<li class="nav-item">
			 			  <a class="nav-link <?php if($keylang == 0): ?> active <?php endif; ?>" data-toggle="tab" href="#sec<?php echo $lan->{'id'} ?>">Titulo <?php echo $lan->{'titulo'} ?></a>
			 			</li>
			 		<?php endforeach; ?>
			 	</ul>
			 	<!-- Tab panes -->
			 	<div class="tab-content">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<div class="tab-pane lang-buttons <?php if($keylang == 0): ?> active <?php endif; ?>" id="sec<?php echo $lan->{'id'} ?>">
				 			
							
			 				<div class="td-input">
								<b>T&iacute;tulo:</b><br>
								<input type="text" name="titulo_<?php echo $lan->{'abr'} ?>" id="titulo" value="<?php if(isset($info[$keylang]->{'titulo'})) echo $info[$keylang]->{'titulo'} ?>">
							</div>
							
			 				<div class="td-input">
								<b>Descripci&oacute;n:</b><br>
								<input type="text" name="description_<?php echo $lan->{'abr'} ?>" id="description" value="<?php if(isset($info[$keylang]->{'description'})) echo $info[$keylang]->{'description'} ?>">
							</div>
				 			
					        <div class="td-input">
<!--		    					<input type="text" name="title_<?php echo $lan->{'abr'} ?>" id="title" placeholder="Titulo <?php echo $lan->{'titulo'} ?>" <?php if($info[$keylang]->{'lang'} == $lan->{'id'}): ?> value="<?php echo $info[$keylang]->{'nombre'} ?>" <?php endif; ?>>-->
		    					<input type="hidden" name="id_idioma_<?php echo $lan->{'abr'} ?>" value="<?php echo $lan->{'id'} ?>">
		    					<!--<input type="hidden" name="id_columna" value="<?php echo $info[$keylang]->{'id'} ?>">-->
		    				</div>

			 			</div>
			 		<?php endforeach; ?>
                    
                        
        				<div class="td-input">
        					<b>Imagen de producto:</b><br>
        					<input type="text" name="galeria1_input" id="galeria2_input" class="img-input" value="<?php echo $info[0]->{'background'}?>" readonly>
        					<div id="main_uploader">
        						<div class="uploader-id2">
        							<div id="uploader2" align="left">
        								<input id="uploadify2" type="file" class="uploader" />
        							</div>
        						</div>
        						<div id="filesUploaded" style="display:none;"></div>
        						<div id="thumbHeight2" style="display:none;" >800</div>
        						<div id="thumbWidth2" style="display:none;" >1440</div>
        					</div>
        					<div id="galeria2" class="upload-galeria">
        						<?php if($info[$keylang]->{'background'}<>''){ ?>
        						<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../../asset/img/uploads/<?php echo $info[$keylang]->{'background'}?>',function(){}); $('#galeria2_input').val(''); $(this).parent().remove();"></div><img src="../../../../asset/img/uploads/<?php echo $info[$keylang]->{'background'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
        						<?php } ?>
        					</div>
        				</div>
                        <div class="td-input">
                        	<b>Icono:</b><br>
                        	<input type="text" name="galeria2_input" id="galeria3_input" class="img-input" value="<?php echo $info[0]->{'icono'}?>" readonly>
                        	<div id="main_uploader">
                        		<div class="uploader-id3">
                        			<div id="uploader3" align="left">
                        				<input id="uploadify3" type="file" class="uploader" />
                        			</div>
                        		</div>
                        		<div id="filesUploaded" style="display:none;"></div>
                        		<div id="thumbHeight3" style="display:none;" >800</div>
                        		<div id="thumbWidth3" style="display:none;" >1440</div>
                        	</div>
                        	<div id="galeria3" class="upload-galeria">
                        		<?php if($info[$keylang]->{'icono'}<>''){ ?>
                        		<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../../asset/img/uploads/<?php echo $info[$keylang]->{'icono'}?>',function(){}); $('#galeria3_input').val(''); $(this).parent().remove();"></div><img src="../../../../asset/img/uploads/<?php echo $info[$keylang]->{'icono'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
                        		<?php } ?>
                        	</div>
                        </div>
			 		</div>
			 	</div>

				   
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	   <a href="<?php echo base_url()?>productos/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
	</div>
</div>
<br style="clear:both;"/>  
<script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js"></script> 
<script type="text/javascript" src="<?php echo base_url() ?>asset/js/advanced.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery.uploadifive.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	/*UPLOAD CONFIG*/
	var w=1440;
	var h=810;
	var path='../../../../asset/img/uploads/';
	
	
	var maxWidth=1440;
	var thWidth=1440;
	var thHeight=810;		
	var tipo='unica';
	var allowedTypes='jpg,png,gif,svg'
	var callback=function(){console.log('upload complete');}

		uploaderNoCrop('2',path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);
		uploaderNoCrop('3',path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);
	});
</script>
