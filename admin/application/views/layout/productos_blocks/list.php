<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:10px;">
		<div class="listado">
			<div class="col-md-12 home-tools">
				<div class="row">
					<div class="col-xs-6 col-md-6">
						<h2>BLOQUES DE PRODUCTO</h2>
					</div>
					<div class="col-xs-6 col-md-6">
						<a href="<?php echo base_url()?>productos_blocks/select_module/"><div class="btn btn-success btn-sm bt-save pull-right" style="margin-right:8px;">AGREGAR NUEVO</div></a>
						<a href="<?php echo base_url()?>productos/"><div class="btn btn-warning btn-sm bt-save pull-right" style="margin-right:8px;">VOLVER</div></a>
					</div>
				</div>
			</div>
			
			
			<!-- info -->
				<hr>
				<hr>
				<hr>
			
			
			<table id="list" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>	
						<th width="40">ID</th>
						<th>Imagen</th>
						<th>M&oacute;dulo</th>
						<th>ORDEN</th>
						<th width="40">Configurar</th>
						<th width="40">Eliminar</th>
					</tr>
				</thead>
				<tbody class="sortable">
					<?php
						$html='';
						foreach ( $info as $fila ){
						
						   if($fila->{"user_name"} != "admin"):
						
							$html.='<tr class="list-sort" id="'.$fila->{'id'}.'">
								<td>'.$fila->{'id'}.'</td>
								<td><img src="'.base_url().'/asset/img/uploads/'.$fila->image.'" style="width:90px"></td>
								<td><b>'.$fila->{'modulo'}.'</b></td>
								<td style="text-align:center;font-size:15px"><b>'.$fila->{'orden'}.'</b></td>
								<td align="center"><a style="color:red; font-size:12px; font-weight:bold" href="'.base_url().'productos_blocks/configure_block/'.$fila->{'id'}.'/?lang=5">[-Config-]</a></td>
								<td align="center"><a href="#" data-href="'.base_url().'productos_blocks/remove/'.$fila->{'id'}.'/" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
							</tr>';
							
							endif;
						}
						echo $html;
					?>				
				</tbody>
			</table>
		</div>
	</div>
</div>
<br style="clear:both;"/>